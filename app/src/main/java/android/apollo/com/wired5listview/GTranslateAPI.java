package android.apollo.com.wired5listview;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jsoup.Jsoup;

/**
 * Created by celil on 30.01.2018.
 */

public class GTranslateAPI {

    public static String getTurkish(String word){
        return parseJson(translateToTurkish(word));
    }

    private static String translateToTurkish(String word) {
        String url = "https://translation.googleapis.com/language/translate/v2?q=" + word + "&target=tr&key=AIzaSyB_mG7bQiEFAqkr-7CDmaw6Cbw5obfc880";

        String result = "";
        try {
            result = Jsoup.connect(url).ignoreContentType(true).execute().body();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    private static String parseJson(String jsonContent) {
        JsonElement jelement = new JsonParser().parse(jsonContent);
        JsonObject jobject = jelement.getAsJsonObject();
        jobject = jobject.getAsJsonObject("data");
        JsonArray jarray = jobject.getAsJsonArray("translations");
        jobject = jarray.get(0).getAsJsonObject();
        String result = jobject.get("translatedText").getAsString();
        return result;
    }

    public static void main(String[] args) {
        String resultJson = GTranslateAPI.translateToTurkish("computer");
        System.out.println(parseJson(resultJson));
    }
}
