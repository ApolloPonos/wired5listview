package android.apollo.com.wired5listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by celil on 29.01.2018.
 */

public class NewsAdaptor extends BaseAdapter {

    private Context context;
    private List<News> newsList;

    public NewsAdaptor(Context context, List<News> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @Override
    public int getCount() {
        return this.newsList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.newsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);

        News news = this.newsList.get(i);

        View viewLocal = layoutInflater.inflate(R.layout.listview_news, null);
        TextView newsContent = (TextView) viewLocal.findViewById(R.id.newsContent);

        newsContent.setText(news.getTitle());

        return viewLocal;
    }
}
