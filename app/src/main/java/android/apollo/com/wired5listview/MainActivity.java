package android.apollo.com.wired5listview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends Activity {

    ListView newsListView;
    List<News> newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //newsList = News.generateFakeNews();
        newsList = NewsRetriever.getLatestNews(5);

        NewsAdaptor newsAdaptor = new NewsAdaptor(this, newsList);

        newsListView = (ListView) findViewById(R.id.newsListView);
        newsListView.setAdapter(newsAdaptor);

        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                    News news = newsList.get(i);

                                                    Intent it = new Intent(MainActivity.this, NewsActivity.class);
                                                    it.putExtra("news", news);
                                                    startActivity(it);
                                                }
                                            }

        );

    }

}