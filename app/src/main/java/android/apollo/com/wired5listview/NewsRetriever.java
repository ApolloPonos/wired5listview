package android.apollo.com.wired5listview;

import android.content.Context;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * Created by celil on 29.01.2018.
 */

public class NewsRetriever {


    public static List<News> getLatestNews(int i) {

        List<News> latestNews = new ArrayList<>();
        String wiredUrl = "https://www.wired.com/feed/rss";
        try {
            Document xmlDoc = Jsoup.parse(Jsoup.connect(wiredUrl).get().toString(), wiredUrl, Parser.xmlParser());
            for (int j = 0; j < i; j++) {
                Elements elements = xmlDoc.select("channel").select("item");

                String title = elements.get(j).select("title").text();
                Date publishDate = new Date(elements.get(j).select("pubDate").text());
                String author = elements.get(j).select("author").text();
                String link = elements.get(j).select("link").text();

                Document content = Jsoup.connect(link).get();

                String contentRaw = content.toString();
                String contentClean = content.text();

                ArrayList<String> topWordList = getTopWords(contentClean);

                News news = new News(title, contentRaw, new Date(), author, link);
                news.setTopVocabularyList(topWordList);

                Map<String, String> translationMap = generateWordTranslationMap(topWordList);
                news.setTopVocabularyTranslationMap(translationMap);

                latestNews.add(news);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return latestNews;
    }

    private static Map<String, String> generateWordTranslationMap(ArrayList<String> topWord) {

        Map<String, String> result = new HashMap<>();

        for (String word : topWord) {
            result.put(word, GTranslateAPI.getTurkish(word));
        }

        return result;
    }

    private static ArrayList<String> getTopWords(String content) {

        List<String> topWordList = new ArrayList<>();

        String[] contentArray = content.split("\\s+");
        Map<String, Integer> contentMap = new HashMap<>();

        for (int i = 0; i < contentArray.length; i++) {
            if (contentMap.containsKey(contentArray[i])) {
                contentMap.put(contentArray[i], contentMap.get(contentArray[i]) + 1);
            } else {
                contentMap.put(contentArray[i], 1);
            }
        }

        Set<String> contentKeys = contentMap.keySet();
        Map<String, Integer> topWordMap = new ConcurrentHashMap<>();

        for (String key : contentKeys) {
            Set<String> topSet = topWordMap.keySet();

            if (topSet.size() < 5) {
                topWordMap.put(key, contentMap.get(key));
            } else {
                Set<String> topWordKeys = topWordMap.keySet();
                for (String topKey : topWordKeys) {
                    if (contentMap.get(key) > topWordMap.get(topKey)) {
                        topWordMap.remove(topKey);
                        topWordMap.put(key, contentMap.get(key));
                    }
                }
            }
        }

        return new ArrayList<>(topWordMap.keySet());
    }


    public static void main(String[] args) {

        List<News> latestNews = getLatestNews(5);
        for (News news : latestNews) {
            System.out.println(news.getTitle());
            ArrayList<String> topWords = news.getTopVocabularyList();
            for (String word : topWords) {
                System.out.println("EN: " + word + ", TR: " + GTranslateAPI.getTurkish(word));
            }
        }
    }
}
