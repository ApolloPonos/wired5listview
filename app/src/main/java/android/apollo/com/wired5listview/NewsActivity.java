package android.apollo.com.wired5listview;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Map;
import java.util.Set;

/**
 * Created by celil on 29.01.2018.
 */

public class NewsActivity extends AppCompatActivity{
    WebView content;
    TextView translated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        News news = (News) getIntent().getExtras().get("news");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(news.getTitle());

        content = (WebView) findViewById(R.id.contentNews);
        //content.loadUrl(news.getLink());
        content.loadData(news.getContent(), "text/html", null);

        translated = (TextView) findViewById(R.id.translateNews);
        Map<String, String> translationMap = news.getTopVocabularyTranslationMap();
        Set<String> keyset = translationMap.keySet();

        String translatedString = "";
        for (String key: keyset){
            translatedString = translatedString + "" + key +"=>" + translationMap.get(key) + "|";
        }

        translated.setText(translatedString);

    }

}
