package android.apollo.com.wired5listview;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class News implements Serializable {

    private String title;
    private String content;
    private Date publishDate;
    private String author;
    private String link;
    private ArrayList<String> topVocabularyList;
    private Map<String, String> topVocabularyTranslationMap;

    public News(String title, String content, Date publishDate, String author, String link) {
        this.title = title;
        this.content = content;
        this.publishDate = publishDate;
        this.author = author;
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArrayList<String> getTopVocabularyList() {
        return topVocabularyList;
    }

    public void setTopVocabularyList(ArrayList<String> topVocabularyList) {
        this.topVocabularyList = topVocabularyList;
    }

    public Map<String, String> getTopVocabularyTranslationMap() {
        return topVocabularyTranslationMap;
    }

    public void setTopVocabularyTranslationMap(Map<String, String> topVocabularyTranslationMap) {
        this.topVocabularyTranslationMap = topVocabularyTranslationMap;
    }

    public static List<News> generateFakeNews(){
        List<News> fakeNewsList = new ArrayList<>();
        fakeNewsList.add(new News("celil1title",  "celil1content", new Date(), "celil1author", "celil1link"));
        fakeNewsList.add(new News("celil2title",  "celil2content", new Date(), "celil2author", "celil2link"));
        fakeNewsList.add(new News("celil3title",  "celil3content", new Date(), "celil3author", "celil3link"));
        fakeNewsList.add(new News("celil4title",  "celil4content", new Date(), "celil4author", "celil4link"));
        fakeNewsList.add(new News("celil5title",  "celil5content", new Date(), "celil5author", "celil5link"));

        return fakeNewsList;
    }
}
